import json

import requests

from src.main.config.config import *


class qclient:
    @staticmethod
    def do_prediction(n_estimators, random_state,
                      max_depth, clf_type, training_data, label, test_data):
        request_dict = dict()

        if not isinstance(n_estimators, int):
            raise TypeError('n_estimators expects integer')
        else:
            request_dict[N_ESTIMATORS] = n_estimators

        if not isinstance(random_state, int):
            raise TypeError('random_estimators expects integer')
        else:
            request_dict[RANDOM_STATE] = random_state

        if not isinstance(max_depth, int):
            raise TypeError('max_depth expects integer')
        else:
            request_dict[MAX_DEPTH] = max_depth

        if clf_type not in CLF_LIST:
            raise TypeError(clf_type, " is not in classifier list, expected classifiers are: ", CLF_LIST)
        else:
            request_dict[CLF] = clf_type

        data_object = dict()
        if not isinstance(training_data, list):
            raise TypeError('train_data expects a list of list floats')
        if not all(isinstance(x, list) for x in training_data):
            raise TypeError('train_data expects a list of list floats')
        for first_list in training_data:
            for x in first_list:
                if not isinstance(x, (float, int)):
                    raise TypeError('training_data expects list of list floats')
        data_object[TRAINING_DATA] = training_data

        if not isinstance(label, list):
            raise TypeError('label expects a list')
        if not all(isinstance(x, (float, int)) for x in label):
            raise TypeError('train_data expects a list with all floats')
        data_object[LABEL] = label

        if not isinstance(test_data, list):
            raise TypeError('test_data expects a list of list floats')
        if not all(isinstance(x, list) for x in test_data):
            raise TypeError('test_data expects a list of list floats')
        for first_list in test_data:
            for x in first_list:
                if not isinstance(x, (float, int)):
                    raise TypeError('test_data expects list of list floats')
        data_object[TEST_DATA] = test_data

        request_dict[DATA] = json.dumps(data_object)
        headers = {'content-type': 'application/json'}

        print (request_dict)
        r = requests.post(SERVICE_URL, json=request_dict, headers=headers)
        print(r.text)


if __name__ == "__main__":
    qclient.do_prediction(35, 9527, 3, 'ADA_BOOST',
                          [[-0.11081911623478, 1.4895435571671], [1.4190672636032, -1.1671440601349],
                           [-1.4018576145172, 0.26720643043518], [0.093609392642975, -0.58960610628128]],
                          [-1, 1, -1, -1], [[2.0806338787079, -0.41520032286644]])
