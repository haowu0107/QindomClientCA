# service info
#SERVICE_URL = 'http://18.191.139.67/QIT-1.0.0/qit/wangbo'
SERVICE_URL = 'http://localhost:8080/qit/wangbo'
SERVICE_PORT = ''

#post parameters
N_ESTIMATORS = 'n_estimators'
RANDOM_STATE = 'random_state'
MAX_DEPTH = 'max_depth'
DATA = 'data'
METHOD = 'POST'
CLF = 'clf'
TRAINING_DATA = 'X'
LABEL = 'y'
TEST_DATA = 'X_test'
DATA = 'data'

#clf options:
CLF_LIST = ['QBOOST_IT', 'QBOOST', 'ADA_BOOST', 'RANDOM_FOREST', 'WEAK_CLASSIFIER']

#default batch size
DEFAULT_BATCH_SIZE = 4

